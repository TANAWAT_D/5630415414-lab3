/**
 *	Program name : MatchingPenny
 *	@author: Tanawat Dathtanim
 **/
package dathtanim.tanawat.lab3;

import java.util.Scanner;

public class MatchingPenny {

	static String HEAD = "head";
	static String TAIL = "tail";
	static int MIN = 0;
	static int MAX = 1;

	public static void main(String[] args) {

		int random = MIN + (int) (Math.random() * ((MAX - MIN) + 1));
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter head or tail: ");
		String inputConsole = scan.next();

		if (inputConsole.equalsIgnoreCase(HEAD) || inputConsole.equalsIgnoreCase(TAIL)) {

			if (random == 0) {
				System.out.println("You play " + HEAD);
				System.out.println("Computer plays " + HEAD);
				if (inputConsole.equalsIgnoreCase(HEAD))
					System.out.println("You win.");
				if (inputConsole.equalsIgnoreCase(TAIL))
					System.out.println("Computer wins.");
			}
			if (random == 1) {
				System.out.println("You play " + TAIL);
				System.out.println("Computer plays " + TAIL);
				if (inputConsole.equalsIgnoreCase(HEAD))
					System.out.println("Computer wins.");
				if (inputConsole.equalsIgnoreCase(TAIL))
					System.out.println("You win.");
			}

		} else
			System.err.println("Incorrect input. head or tail only");
		scan.close();
	}

}
