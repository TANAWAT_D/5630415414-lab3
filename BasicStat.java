/**
 *	Program name : BasicStat
 *	@author: Tanawat Dathtanim
 **/
package dathtanim.tanawat.lab3;

import java.util.Arrays;

public class BasicStat {

	static double[] num;
	static int numInput;
	static int length;
	static double sum = 0;
	static double avg;
	static double sd;

	public static void main(String[] args) {

		acceptInput(args);
		displayStats();

	}

	static void acceptInput(String[] args) {

		length = Integer.parseInt(args[0]);
		num = new double[length];

		if (args.length != length + 1) {

			System.err.println("<BasicStat> <numNumbers> <numbers>...");

		} else {

			for (int i = 0; i < length; i++) {
				num[i] = Double.parseDouble(args[i + 1]);
				sum = sum + Double.parseDouble(args[i + 1]);
			}
			Arrays.sort(num);
			avg = sum / length;
			sum = 0;

			for (int i = 0; i < length; i++) {
				sum = sum + Math.pow(num[i] - avg, 2);
			}
			sd = Math.sqrt(sum / length);

		}

	}

	static void displayStats() {

		System.out.println("Max is " + num[length - 1] + " Min is " + num[0]);
		System.out.println("Average is " + avg);
		System.out.println("Standard Deviation is " + sd);

	}

}
