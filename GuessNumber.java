/**
 *	Program name : GuessNumber
 *	@author: Tanawat Dathtanim
 **/
package dathtanim.tanawat.lab3;

import java.util.Scanner;

public class GuessNumber {

	static int answer;
	static int numOgGuess = 7;
	static int num = 7;

	public static void main(String[] args) {
		answer = genAnswer();
		runGame();

	}

	static int genAnswer() {
		int min = 0;
		int max = 100;
		int random = min + (int) (Math.random() * ((max - min) + 1));
		return random;
	}

	static void runGame() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Guess anumber between 1 to 100.");
		for (int i = 0; i < num; i++) {

			System.out.println("Number of remaining guess is " + numOgGuess);
			System.out.print("Guess a number: ");
			String inputConsole = scan.next();
			numOgGuess--;
			int input = Integer.parseInt(inputConsole);
			if (input == answer) {
				System.out.println("Correct!");
				System.exit(0);
			} else if (input < answer) {
				System.out.println("Higher");

			} else {
				System.out.println("Lower!");

			}
			if (numOgGuess == 0) {
				System.out.println("You ran out of guess.The number was " + answer);
			}
		}
		scan.close();

	}

}
