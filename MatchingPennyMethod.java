/**
 *	Program name : MatchingPennyMethod
 *	@author: Tanawat Dathtanim
 **/
package dathtanim.tanawat.lab3;

import java.util.Scanner;

public class MatchingPennyMethod {

	static String humanChoice;
	static String compChoice;
	static String HEAD = "head";
	static String TAIL = "tail";
	static int MIN = 0;
	static int MAX = 1;

	public static void main(String[] args) {

		humanChoice = acceptInput();
		compChoice = genComChoice();
		displayWinner(humanChoice, compChoice);

	}

	static String acceptInput() {

		Scanner scan = new Scanner(System.in);

		System.out.print("Enter head or tail: ");
		String inputConsole = scan.next();

		if (inputConsole.equalsIgnoreCase(HEAD)) {
			System.out.println("You play " + HEAD);
		} else if (inputConsole.equalsIgnoreCase(TAIL)) {
			System.out.println("You play " + TAIL);
		} else {
			System.err.println("Incorrect input. head or tail only");
			System.exit(0);
		}
		scan.close();
		return inputConsole.toLowerCase();
	}

	static String genComChoice() {

		String choice = "";

		int random = MIN + (int) (Math.random() * ((MAX - MIN) + 1));

		if (random == 0) {
			System.out.println("Computer plays head");
			choice = HEAD;
		}
		if (random == 1) {
			System.out.println("Computer plays tail");
			choice = TAIL;
		}
		return choice;
	}

	static void displayWinner(String humanChoice, String compChoice) {

		if (humanChoice.equals(compChoice)) {
			System.out.println("You win.");
		} else
			System.out.println("Computer wins.");

	}

}
